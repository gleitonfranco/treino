package br.com.caelum.vraptor.blank.dao;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import br.com.caelum.vraptor.blank.model.Empregado;
import br.com.caelum.vraptor.ioc.Component;
import br.com.caelum.vraptor.ioc.SessionScoped;

@Component
@SessionScoped
public class EmpregadoDao {

	private List<Empregado> empregados;
	private Long idMax = 6L;

	public EmpregadoDao() {
		this.empregados = new ArrayList<Empregado>();
		this.empregados.add(new Empregado(1l, "André"));
		this.empregados.add(new Empregado(2l, "Gleiton"));
		this.empregados.add(new Empregado(3l, "Lázaro"));
		this.empregados.add(new Empregado(4l, "Ramiro"));
		this.empregados.add(new Empregado(5l, "Ivo"));
	}

	public Empregado carrega(Long id) {
		for (Empregado e : this.empregados) {
			if (id.equals(e.getId())) {
				return e;
			}
		}
		return null;
	}

	public void salvarOuAtualizar(Empregado novo) {
		if (novo.getId() == null) {
			novo.setId(this.idMax++);
			this.empregados.add(novo);
		} else {
			int index = -1;
			for (Empregado e : this.empregados) {
				if (novo.equals(e)) {
					index = this.empregados.indexOf(e);
					break;
				}
			}
			if (index >= 0) this.empregados.set(index, novo);
		}
	}

	public void remover(Empregado empregado) {
		this.empregados.remove(empregado);
	}

	public List<Empregado> todos() {
		return this.empregados;
	}

	public List<Empregado> pesquisar(Long id, String nome) {
		final List<Empregado> resultado = new ArrayList<Empregado>();
		for (Empregado e : this.empregados) {
			if (
					(id != null && id.equals(e.getId())) 
					||
					(nome != null && e.getNome().toLowerCase().contains((nome.toLowerCase())))
				) {
				resultado.add(e);
			}
		}
		return resultado;
	}

}
