package br.com.caelum.vraptor.blank.controller;

import java.util.ArrayList;
import java.util.List;

import br.com.caelum.vraptor.Post;
import br.com.caelum.vraptor.Resource;
import br.com.caelum.vraptor.Result;
import br.com.caelum.vraptor.blank.dao.EmpregadoDao;
import br.com.caelum.vraptor.blank.model.Empregado;

@Resource
public class EmpregadosController {

	private final EmpregadoDao dao;
	private final Result result;

	public EmpregadosController(EmpregadoDao dao, Result result) {
		this.dao = dao;
		this.result = result;
	}

	public List<Empregado> list(List<Empregado> list) {
		return (list == null) ? dao.todos() : list;
	}
	
	@Post
	public void salvar(Empregado empregado) {
		this.dao.salvarOuAtualizar(empregado);
		this.result.redirectTo(EmpregadosController.class).list(null);
	}

	public Empregado form(Long id) {
		if (id != null) {
			return this.dao.carrega(id);
		}
		return new Empregado();
	}
	
	public void novo() {
		this.result.redirectTo(EmpregadosController.class).form(null);
	}
	
	public Empregado remover(Long id) {
		Empregado e = this.dao.carrega(id);
		this.dao.remover(e);
		return e;
	}
	
	public void pesquisar(Long id, String nome) {
		System.out.println(id);
		System.out.println(nome);
		List<Empregado> p = dao.pesquisar(id, nome);
		System.out.println(p);
		this.result.include("list", p);
		this.result.redirectTo(EmpregadosController.class).list(p);
	}
	
}
