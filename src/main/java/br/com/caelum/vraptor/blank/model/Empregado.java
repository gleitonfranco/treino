package br.com.caelum.vraptor.blank.model;

public class Empregado {

	private Long id;

	private String nome;
	
	public Empregado() {
		
	}
	
	public Empregado(Long id, String nome) {
		super();
		this.id = id;
		this.nome = nome;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	@Override
	public boolean equals(Object obj) {
		Long id = ((Empregado)obj).getId();
		if (id == null) {
			return false;
		}
		return id.equals(this.getId());
	}

	@Override
	public String toString() {
		return this.id.toString() + " - " + this.nome;
	}
	
	

}
