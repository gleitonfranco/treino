<%-- <%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %> --%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Empregados</title>
</head>
<body>
    <div class="container jumbotron">
        <h2>Formulário de Funcionários</h2>
        <form action="/treino/empregados/salvar" method="post">
            <div class="form-group">
		        <label for="id">ID:</label>
	            <input name="empregado.id" value="${empregado.id}" type="text" readonly class="form-control" id="id"/>
            </div>
            <div class="form-group">
	            <label for="nome">NOME:</label>
	            <input name="empregado.nome" value="${empregado.nome}" type="text" class="form-control" id="nome"/>
            </div>
            <button type="submit" class="btn btn-primary">Salvar</button>
        </form>
        <a href="/treino/empregados/list" class="btn btn-success">Voltar</a>
    </div>

     <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <!-- jQuery library -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <!-- Latest compiled JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script> 
    
</body>
</html>