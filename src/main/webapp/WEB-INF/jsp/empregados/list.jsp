<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Empregados</title>
</head>
<body>
    <div class = "container jumbotron">
        <h1>Lista de Funcion�rios</h1>
        <form action="/treino/empregados/pesquisar" method="post">
        <div class="form-group">
            <label for="id">ID:</label>
            <input name="id" value="${id}" type="text" class="form-control" id="id"/>
        </div>
        <div class="form-group">
            <label for="nome">NOME:</label>
            <input name="nome" value="${nome}" type="text" class="form-control" id="nome"/>
        </div>
        <button type="submit" class="btn btn-info">Pesquisar</button>
        <a href="/treino/empregados/novo" class="btn btn-primary" >Novo</a>
        </form>
        <table class="table table-dark" id="tbfunc">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>NOME</th>
                </tr>
            </thead>
            <tbody>
                <c:forEach var="empregado" items="${empregadoList}">
                    <tr>
                        <td>${empregado.id }</td>
                        <td>${empregado.nome }</td>
                        <td><a href="/treino/empregados/form?id=${empregado.id}" class="btn btn-success">Editar</a></td>
                        <td><a href="/treino/empregados/remover?id=${empregado.id}" data-confirm="Voc� tem certeza???:o" data-method="delete" class="btn btn-danger">Remover</a></td>
                    </tr>
                </c:forEach>
            </tbody>
        </table>
    </div>

     <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <!-- jQuery library -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <!-- Latest compiled JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script> 
    
</body>
</html>