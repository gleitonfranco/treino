<%-- <%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %> --%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Empregados</title>
</head>
<body>
    <div class = "container jumbotron">
        <h2>Funcionário ID=${empregado.id} - ${empregado.nome} removido com sucesso!</h2>
        <a href="/treino/empregados/list" class="btn btn-success">Voltar</a>
    </div>

     <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <!-- jQuery library -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <!-- Popper JS -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
    <!-- Latest compiled JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script> 
    <!-- Backbone JS -->
    <script type="application/javascript" src="https://underscorejs.org/underscore-min.js"></script>
<!--     <script type="application/javascript" src="/treino/js/list.js" ></script> -->
    
</body>
</html>