<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>

<center>Cadastro de Funcionário</center>
              <form action="<c:url value="/funcionarios"/>" method="POST">
                 <div class="form-group">
                   <label for="exampleInputNome">Nome</label>
                     <input id="nome" type="text" class="form-control" id="exampleInputEmail1" aria-describedby="nomeHelp" placeholder="Entre com o  Nome" name="funcionario.nome" required oninvalid="this.setCustomValidity('Por favor, preencha o NOME!')" onchange="try{setCustomValidity('')}catch(e){}" />
                     <br>
                     <button type="submit" class="btn btn-primary">Cadastrar</button>
                     <button type="reset" class="btn btn-secondary">Cancelar</button>
                </div>
              </form>

</body>
</html>