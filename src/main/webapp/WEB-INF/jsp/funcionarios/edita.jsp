<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<html>
<head>
<title>..::Treinamento - Marcus Paulo::..</title>
<link href="<c:url value="/goodbuy.css"/>" rel="stylesheet" type="text/css" media="screen" />
<link href="<c:url value="/javascripts/jquery.autocomplete.css"/>" rel="stylesheet" type="text/css" media="screen" />
<script type="text/javascript" src="<c:url value="/javascripts/jquery-1.3.2.min.js"/>"></script>
<script type="text/javascript" src="<c:url value="/javascripts/jquery.validate.min.js"/>"></script>
<script type="text/javascript" src="<c:url value="/javascripts/jquery.autocomplete.min.js"/>"></script>
<script type="text/javascript" src="<c:url value="/javascripts/jquery.puts.js"/>"></script>
<script type="text/javascript" src="<c:url value="/javascripts/main.js"/>"></script>
<link href="https://getbootstrap.com.br/docs/4.1/dist/css/bootstrap.css" rel="stylesheet">
<fmt:setLocale value="pt_br" />
</head>
<body>
	<div id="header" class="nav-item">
		<center>Altera��o de Funcion�rios</center>
		<br>
		<form action="<c:url value="/funcionarios/${funcionarios.id }"/>" method="POST">
			<div class="form-group">
				<label for="exampleInputId">C�digo</label> 
				<input type="hidden" name="funcionario.id" value="${funcionario.id }" /> 
				<input type="text" name="id" class="form-control" value="${funcionario.id }" disabled="disabled"/>  
				 
				<label for="exampleInputNome">Nome</label> 
				<input id="nome" type="text" name="funcionario.nome" value="${funcionario.nome }" class="form-control" placeholder="Entre com o Nome" required oninvalid="this.setCustomValidity('Por favor, preencha o NOME!')" onchange="try{setCustomValidity('')}catch(e){}" />
				<br><br>
	
				<button type="submit" class="btn btn-primary" name="_method" value="PUT">Alterar</button>
				<button type="reset" class="btn btn-secondary">Cancelar</button>
				<a href="http://localhost:8080/treina/funcionarios"><button type="button" class="btn btn-info">Retornar</button></a></td>
			</div>
		</form>

	</div>
	<div id="erros">
		<ul>
			<c:forEach items="${errors}" var="error">
				<li>${error.category }-${error.message }</li>
			</c:forEach>
		</ul>
	</div>
</body>
</html>



