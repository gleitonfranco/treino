<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<%@include file="/header.jspf"%>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link href="https://getbootstrap.com.br/docs/4.1/dist/css/bootstrap.css" rel="stylesheet">

<title>Listando...</title>

</head>
<body>
	<table class="table">
	
		 <thead class="thead-dark">
			<tr>
				<th scope="col">C�digo</th>
				<th>Nome</th>
				<th>Editar</th>
				<th>Excluir</th>
				
			</tr>
		</thead>
		<tbody>
			<c:forEach items="${funcionarioList}" var="funcionario">
			      <tr>
			         <td>${funcionario.id}</td>
				     <td>${funcionario.nome}</td>
				     <td><a href="<c:url value="/funcionarios/${funcionario.id}"/>"><img src="https://cdn4.iconfinder.com/data/icons/simplicio/128x128/document_edit.png"  width="25" height="25" title="Editar"></a></td>
				     <td><form action="<c:url value="/funcionarios/${funcionario.id}"/>"method="POST">
                             <button class="link" name="_method" value="DELETE" onclick='return pergunta()';><img src="http://agnesday.com/wp-content/uploads/2012/05/deletered.png" width="25" height="25" title="Excluir"> </button>
                         </form>
        		     </td>
				     
                  </tr>
			</c:forEach>
		
		
		</tbody>
		
	</table>
	<%@include file="/footer.jspf"%>  
		        
</body>
</html>