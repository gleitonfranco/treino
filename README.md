# README #

### Para que? ###

* Este é um projeto básico com vraptor versão 3.5.3, aonde a partir do mesmo é para o colaborador desenvolver as telas de cadastro, edição e listagem conforme documentação do treinamento da Health Service.

### Como subir o serviço? ###

* Clone o projeto.
* rode o comando `mvn eclipse:eclipse`.
* importe no eclipse como um projeto GIT.
* adicione a um tomcat(no proprio eclipse)
* rode-o no browser acessando a utl: http://localhost:8080/treina. Deverá aparecer o index.jsp com o texto "O projeto vraptor-treina está no ar."
# vraptor-treina